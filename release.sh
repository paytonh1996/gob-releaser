#!/bin/bash
set -e

help() {
    cat <<EOF
---
release.sh helps create code updates and related MRs for releasing tagged versions of GitLab Observability Backend

The following flags are required.
    --environment               One of "staging" or "prod", that targets which environment we're cutting a release for.
                                Use "all" to target both environments.

The following flags are optional.
    --goui-image                Path of the GitLab Observability UI docker image to bump to.

    --existing-clone-path       Path to an existing checkout of the code specific to the environment being updated.
                                When omitted, a new clone is created everytime this script runs.

    --release-version           Name of the tagged release version we want the code to updated to.
                                When omitted, we fetch the latest tag available on the GOB repository.

    --debug                     Toggle "set -x" to trace bash commands. Takes no arguments.

The following settings are extracted from environment variables.
    GITLAB_PRIVATE_TOKEN        Private access token to query details for https://gitlab.com/gitlab-org/opstrace/opstrace
    OPS_PRIVATE_TOKEN           Private access token to query details for https://ops.gitlab.net/opstrace-realm/environments
---

EOF

exit 0
}

while [[ $# -gt 0 ]]; do
    case ${1} in
        --environment)
            ENVIRONMENT="$2"
            shift
            ;;
        --goui-image)
            GOUI_IMAGE="$2"
            shift
            ;;
        --existing-clone-path)
            EXISTING_CLONE_PATH="$2"
            shift
            ;;
        --release-version)
            RELEASE_VERSION="$2"
            shift
            ;;
        --debug)
            set -x
            ;;
        *)
            help
            ;;
    esac
    shift
done

if [ -z "$GITLAB_PRIVATE_TOKEN" ]; then
    echo "GITLAB_PRIVATE_TOKEN not set"
    echo "Please set GitLab Private Token as GITLAB_PRIVATE_TOKEN"
    exit 1
fi

if [ -z "$OPS_PRIVATE_TOKEN" ]; then
    echo "OPS_PRIVATE_TOKEN not set"
    echo "Please set Ops Private Token as OPS_PRIVATE_TOKEN"
    exit 1
fi

# setting up environment and necessary variables
if [[ -z "$ENVIRONMENT" ]]; then
    echo "--environment not set, please use one of (staging,prod,all)"
    exit 1
fi

PROJECT_ID="676"
REPO_NAME="observability"
REPO_PATH="git@ops.gitlab.net:opstrace-realm/environments/gcp/observability.git"


# get the latest GOB tag unless overridden
GOB_PROJECT_ID="32149347"
if [[ -z "$RELEASE_VERSION" ]]; then
    LATEST_TAG=$(curl \
        --silent \
        --header "PRIVATE-TOKEN:${GITLAB_PRIVATE_TOKEN}" \
        "https://gitlab.com/api/v4/projects/${GOB_PROJECT_ID}/repository/tags" | \
        jq -r '.[0].name')
    echo "fetched latest tag=${LATEST_TAG}"
else
    LATEST_TAG=$RELEASE_VERSION
fi
echo "> updating code to tagged release: ${LATEST_TAG}"

# setup needed branch
RELEASE_BRANCH_NAME="release-bump-${LATEST_TAG}"
RELEASE_COMMIT_MSG="Bump environment to tagged release version: ${LATEST_TAG}"

CLONED_PATH=""
if [ -z "$EXISTING_CLONE_PATH" ]; then
    # existing clone path not set, setup a new one
    echo "> git cloning ${REPO_PATH}"
    git clone $REPO_PATH
    CLONED_PATH=$REPO_NAME
else
    CLONED_PATH=$EXISTING_CLONE_PATH
fi

echo "> changing directory to ${CLONED_PATH}"
cd $CLONED_PATH

# figure which is the default branch
TARGET_BRANCH=$(curl \
--silent \
--header "PRIVATE-TOKEN:${OPS_PRIVATE_TOKEN}" \
"https://ops.gitlab.net/api/v4/projects/${PROJECT_ID}" | \
jq -r '.default_branch');
echo "> using target branch (default): ${TARGET_BRANCH}"

# check which branch are we branching out of
CURRENT_BRANCH=$(git branch --show-current)
if [[ ${CURRENT_BRANCH} != "${TARGET_BRANCH}" ]]; then
    echo "git tree not set to target branch: ${TARGET_BRANCH}"
    echo "exiting..."
    exit 1
fi

# check if the repository is good to make updates on top of
if [[ $(git status --porcelain --untracked-files=no | wc -l) -gt 0 ]]; then
    echo "git tree status not clean, cannot proceed satisfactorily"
    echo "exiting..."
    exit 1
fi

# check if the branch already exists
if [[ $(git branch --list "${RELEASE_BRANCH_NAME}") ]]; then
    echo "source branch for the new release: ${RELEASE_BRANCH_NAME} already exists"
    echo "exiting..."
    exit 1
fi
git checkout -b "$RELEASE_BRANCH_NAME"

# git tree status is good if we got to here, lets make required updates
REPLACEMENTS=(
    # release version
    "s/release_tag_version = (.*)/release_tag_version = \"${LATEST_TAG}\"/g"
)
# check if we're updating GOUI image as well
if [[ -n "$GOUI_IMAGE" ]]; then
    REPLACEMENTS+=("s/gitlab-observability-ui:(.*)/gitlab-observability-ui:${GOUI_IMAGE}\"/g")
fi
# execute updates
for replacement in "${REPLACEMENTS[@]}"; do
    echo "> updating source code with: ${replacement}"
    if [[ "$ENVIRONMENT" == "staging" ]]; then
        sed -E -i.bak "${replacement}" staging_vars.hcl
    elif [[ "$ENVIRONMENT" == "prod" ]]; then
        sed -E -i.bak "${replacement}" production_vars.hcl
    elif [[ "$ENVIRONMENT" == "all" ]]; then
        sed -E -i.bak "${replacement}" staging_vars.hcl
        sed -E -i.bak "${replacement}" production_vars.hcl
    else
        echo "unknown environment"
        echo "exiting"
        exit 1
    fi
done

# check if the repository already has intended changes and no replacements were made
if [[ $(git status --porcelain --untracked-files=no | wc -l) -eq 0 ]]; then
    echo "no changes were made and/or observed, we're likely at the targeted release already?"
    echo "exiting..."
    exit 1
fi

echo "> checking for & deleting update leftovers if any"
rm *.bak
echo "> adding updates to git commit"
git add -A
echo "> committing changes"
git commit -m "${RELEASE_COMMIT_MSG}"
echo "> pushing branch ${RELEASE_BRANCH_NAME}"
git push origin "$RELEASE_BRANCH_NAME"

GITLAB_USER_ID=$(curl \
--silent \
--header "PRIVATE-TOKEN:${OPS_PRIVATE_TOKEN}" \
"https://ops.gitlab.net/api/v4/user" | \
jq -r '.id');
echo "> assigning to user ID: ${GITLAB_USER_ID}"

# get all the merge request and take a look if there is already one with the same source branch
COUNT_MRS=$(curl \
--silent \
--header "PRIVATE-TOKEN:${OPS_PRIVATE_TOKEN}" \
"https://ops.gitlab.net/api/v4/projects/${PROJECT_ID}/merge_requests?state=opened&source_branch=${RELEASE_BRANCH_NAME}" | jq 'length')

if [ "${COUNT_MRS}" -eq "0" ]; then
    # no existing MR found, let's create a new one
    echo "found no existing MRs with source_branch <${RELEASE_BRANCH_NAME}>"
    # build the description of our new Merge Request
    BODY="{
        \"id\": ${PROJECT_ID},
        \"source_branch\": \"${RELEASE_BRANCH_NAME}\",
        \"target_branch\": \"${TARGET_BRANCH}\",
        \"remove_source_branch\": true,
        \"title\": \"${RELEASE_COMMIT_MSG}\",
        \"assignee_id\":\"${GITLAB_USER_ID}\"
    }";
    echo "> using the following settings for the new MR: $BODY"
    echo "> creating MR..."
    curl \
    --silent \
    --header "PRIVATE-TOKEN:${OPS_PRIVATE_TOKEN}" \
    --header "Content-Type: application/json" \
    -X POST "https://ops.gitlab.net/api/v4/projects/${PROJECT_ID}/merge_requests" \
    --data "${BODY}";
    echo ""
    echo "> opened a new merge request: ${RELEASE_COMMIT_MSG} and assigned to you";
    echo "all good, bye!"
    exit;
fi

echo "an MR from that source branch already exists, check"
echo "exiting..."
exit 1;
